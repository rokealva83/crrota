# -*- coding: UTF-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from openerp import fields, models, api


class ResUsers(models.Model):
    _inherit = "res.users"

    # race = models.Many2one('crrota.race', string='Race')
    # alliance = models.Many2one('crrota.alliance', string='Alliance')
    # union = models.Many2one('crrota.union', string='Union')
    internal_currency = models.Integer(string='Internal Currency', default=0)
    foreign_currency = models.Integer(string='Foreign Currency',default=0)
    real_currency = models.Integer(string='Real Currency',default=0)
    referral_code = models.Char(string='Referral Code',)
    user_luckyness = models.Integer(string='User Luckyness',)
    last_time_check = models.Datetime(string='Last Time Check',)
    last_time_scan_scient = models.Datetime(string='Last Time Scan Scient',)
    premium_account = models.Boolean(string='Premium Account',)
    time_left_premium = models.Datetime(string='Time Left Premium',)

