# -*- coding: UTF-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import api, fields, models


class CrrotaUserCity(models.Model):
    _name = 'crrota.user.city'

    name = fields.Char(string='Name')
    user_id = fields.Many2one('res.users', string='User')
    system_id = fields.Many2one('crrota.system', string='System')
    planet_id = fields.Many2one('crrota.planet', string='Planet')
    x = fields.Integer(string='coordinate X')
    y = fields.Integer(string='coordinate Y')
    z = fields.Integer(string='coordinate Z')
    name_city = fields.Char(default='New City', string='City name')
    city_size_free = fields.Integer(string='City size')
    population = fields.Integer(default=150, string='Population')
    max_population = fields.Integer(default=500, string='Maximum population')
    founding_date = fields.Datetime(string='Foundation date')
    extraction_date = fields.Datetime(string='Extraction date')
    power = fields.Integer(default=0, string='Power')
    use_energy = fields.Integer(default=0, string='Used energy')
    warehouse_id = fields.Many2one('crrota.warehouse', string='Warehouse')
