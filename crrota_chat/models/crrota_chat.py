# -*- coding: utf-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from odoo import api, fields, models, _
from datetime import datetime


class CrrotaChat(models.Model):
    _name = "crrota.chat"

    user_id = fields.Many2one("res.users", string="User")
    text = fields.Char(string="Text")
    time = fields.Datetime(string="Time", default=datetime.today())


class CrrotaChatPrivate(models.Model):
    _name = "crrota.chat.private"

    user_id = fields.Many2one("res.users", string="User")
    recipient_id = fields.Many2one(string="Recipient")
    text = fields.Char(string="Text")
    time = fields.Datetime(string="Time", default=datetime.today())


class CrrotaUserChatOnline(models.Model):
    _name = "crrota.user.chat.online"

    user_id = fields.Many2one("res.users", string="User")
    time = fields.Datetime(string="Time", default=datetime.today())
    user_online = fields.Boolean()