# -*- coding: UTF-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import api, fields, models


class UserVariables(models.Model):
    _name = 'crotta.user.variables'

    name = fields.Char(string='User variables')
    basic_time_build_ship = fields.Integer(string='Basic ship assembly time')
    koef_ship_element_time = fields.Float(
        string='Coefficient of increasing the time of assembly of the ship')
    minimum_scan_time = fields.Integer(string='Minimum scanning time')
    max_turn_assembly_pieces_basic = fields.Integer(
        string='Basic assembly of workpieces')
    max_turn_assembly_pieces_premium = fields.Integer(
        string='Premium queue for assembling blanks')
    max_turn_building_basic = fields.Integer(
        string='Base turn of construction')
    max_turn_building_premium = fields.Integer(
        string='Premium turn of construction')
    max_turn_production_basic = fields.Integer(string='Base production line')
    max_turn_production_premium = fields.Integer(
        string='Premium production line')
    max_turn_scientic_basic = fields.Integer(string='Basic research line')
    max_turn_scientic_premium = fields.Integer(string='Premium research line')
    max_turn_ship_build_basic = fields.Integer(
        string='Basic assembly of ships')
    max_turn_ship_build_premium = fields.Integer(
        string='Premium assembly of ships')
    time_check_new_technology = fields.Integer(
        string='The period of discovery of new technology')
    min_scientic_level = fields.Integer(string='The minimum scientific level')
    tax_per_person = fields.Float(string='Tax')
    koef_price_increace_modern_element = fields.Float(
        string='Price increase factor for modernization')
    time_refill = fields.Integer(string='Time to refuel the fleet')
    time_refill_all_goods = fields.Integer(
        string='Time to refuel the fleet to the full')
    time_verification_resource = fields.Integer(
        string='Time to check extraction of resources')
