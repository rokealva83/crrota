# -*- coding: UTF-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from openerp import fields, models, api


class CrrotaResources(models.Model):
    _name = "crrota.resources"

    name = fields.Char(string='Name')
    default_start_count = fields.Integer(string='Start count')
    sequence = fields.Integer()


class CrrotaMaterial(models.Model):
    _name = "crrota.material"

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    internal_currency = fields.Integer(string='Internal currency price')
    default_start_count = fields.Integer(string='Start count')
    mass = fields.Integer(string='Mass')
    size = fields.Integer(string='Size')
    material_resources_price_ids = fields.One2many(
        'material.resources.price',
        'material_id',
        string='Material Resources Price')


class MaterialResourcesPrice(models.Model):
    _name = "material.resources.price"
    _rec_name = "resource_id"

    resource_id = fields.Many2one('crrota.resources',
                                  string='Resource')
    material_id = fields.Many2one('crrota.material',
                                  string='Material')
    price = fields.Integer(string='Price')


class CrrotaScience(models.Model):
    _name = "crrota.science"

    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    internal_currency = fields.Integer(string='Internal currency price')
    mass = fields.Integer(string='Mass')
    size = fields.Integer(string='Size')
    science_resources_price_ids = fields.One2many(
        'science.resources.price',
        'science_id',
        string='Material Resources Price')


class ScienceResourcesPrice(models.Model):
    _name = "science.resources.price"
    _rec_name = "resource_id"

    resource_id = fields.Many2one('crrota.resources',
                                  string='Resource')
    science_id = fields.Many2one('crrota.science',
                                 string='Science')
    price = fields.Integer(string='Price')
