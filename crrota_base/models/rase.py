# -*- coding: UTF-8 -*-

#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016- Libre Comunication (<>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from odoo import api, fields, models


class CrrotaRace(models.Model):
    _name = 'crrota.race'

    @api.multi
    def name_get(self):
        return [(record.id, record.name) for record in self]
    
    name = fields.Char(default='Race', string='Race name')
    description = fields.Char(string='Description')
    engine_system = fields.Float(string='System engines')
    engine_intersystem = fields.Float(string='Intersystem engines')
    engine_giper = fields.Float(string='Hyperdimensional Motors')
    engine_null = fields.Float(string='Zero-T junction motors')
    generator = fields.Float(string='Generators')
    armor = fields.Float(string='Armor')
    shield = fields.Float(string='Shields')
    weapon_attack = fields.Float(string='Weapon attack')
    weapon_defense = fields.Float(string='Weapon of protection')
    exploration = fields.Float(string='Research')
    disguse = fields.Float(string='Disguise')
    auximilary = fields.Float(string='Devices')
    image = fields.Binary(string='Image')


class CrrotaUnion(models.Model):
    _name = 'crrota.union'

    @api.multi
    def name_get(self):
        return [(record.id, record.name) for record in self]

    name = fields.Char(string='Union name')


class CrrotaAlliance(models.Model):
    _name = 'crrota.alliance'

    @api.multi
    def name_get(self):
        return [(record.id, record.name) for record in self]

    name = fields.Char(string='Alliance name')
    union_id = fields.Many2one('crrota.union', string='Union')





